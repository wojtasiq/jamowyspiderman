﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour {

    public GameObject soundCreatorPref;

    public AudioClip grandma;
    public AudioClip disco;
    public AudioClip elevetor;

    public static Building Data;
    public GameObject windowNormalLightOn;

    public GameObject[] roofPref;
    public GameObject[] facedePref;

    public GameObject[] jambsPrefs;
    public int[] chance;

    private Texture2D buildingMap;
    public IWindow[,] windows;
    private int[,] onlySimleJambs;

    private float xDistance;
    private float yDistance;


    public GameObject doorsUp;
    public GameObject doorDown;

    public GameObject[] bushes;

    private float showingSpeed = 0.2f;

    private Vector2Int endGoal;

    public Vector2Int GetEndGoal()
    {
        return endGoal;
    }

    public int GetBuildingHeight()
    {
        return buildingMap.height;
    }

    public int GetBuildingWidth()
    {
        return buildingMap.width;
    }

    public float GetXDistance()
    {
        return xDistance;
    }

    public float GetYDistance()
    {
        return yDistance;
    }

    private void Start()
    {
        endGoal = new Vector2Int(-10, -10);
        xDistance = yDistance = 2;
        buildingMap = LevelMenager.menager.levels[LevelMenager.menager.CurrenLevel];
        onlySimleJambs = new int[buildingMap.width, buildingMap.height];
        transform.position = new Vector3(-xDistance * (buildingMap.width - 1) / 2, -yDistance * (buildingMap.width - 1) / 2, 0);
        LoadWindowsFromImage();
        LoadBuilding();
        StartCoroutine("ShowBuilding");
        Data = this;
        
    }

    IEnumerator ShowBuilding()
    {
        Camera cam = GameObject.Find("MainCamera").GetComponent<Camera>();
        Transform camPos = GameObject.Find("MainCamera").transform;
        cam.orthographicSize = buildingMap.width - 2;
        camPos.position = new Vector3(0, buildingMap.height/2 + 3, -10);
        yield return new WaitForSeconds(0.5f);
        while (camPos.transform.position.y > transform.position.y)
        {
            camPos.transform.position = new Vector3(camPos.transform.position.x, camPos.position.y - showingSpeed, -10);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(0.5f);
        cam.orthographicSize=CameraMovement.defaultSize;
        GameObject.Find("Player").GetComponent<Player>().letMove();

        cam.transform.GetComponent<CameraMovement>().FollowPlayer();
        camPos.position = new Vector3(0, 0, -25);

    }

    private void LoadBuilding()
    {
        int collectionForce = 0;
        for(int c=0; c<chance.Length; c++)
        {
            collectionForce += chance[c];
            chance[c] = collectionForce;
        }
        transform.position = new Vector3(-xDistance * (buildingMap.width-1)/2 ,-yDistance * (buildingMap.width-1)/2,0);
        for(int i = 0; i < buildingMap.height; i++)
        {
            int rand = Random.Range(0, collectionForce);

            for (int c = 0; c < chance.Length; c++)
            {
                if (rand < chance[c])
                {
                    if (onlySimleJambs[0, i]!=-1)
                    {
                        if (onlySimleJambs[0, i] != 3)
                        {
                            Instantiate(jambsPrefs[c], new Vector3(transform.position.x, transform.position.y + 2 * i, 0), transform.rotation);
                            break;
                        }
                        else
                        {
                            Instantiate(jambsPrefs[2], new Vector3(transform.position.x, transform.position.y + 2 * i, 0), transform.rotation);
                            break;
                        }
                    }
                    else
                    {
                        Instantiate(jambsPrefs[2], new Vector3(transform.position.x, transform.position.y + 2 * i, 0), transform.rotation);
                    }
                }
            }

            Instantiate(facedePref[0], new Vector3(transform.position.x, transform.position.y + 2 * i, 0), transform.rotation);
            for(int j = 1; j < buildingMap.width-1; j++)
            {
                rand = Random.Range(0, collectionForce);
                for (int c = 0; c < chance.Length; c++)
                {
                    if (rand < chance[c])
                    {
                        if (onlySimleJambs[j, i]!=-1)
                        {
                            if (onlySimleJambs[j, i] != 3)
                            {
                                Instantiate(jambsPrefs[c], new Vector3(transform.position.x + j * 2, transform.position.y + 2 * i, 0), transform.rotation);
                                break;
                            }
                            else
                            {
                                Instantiate(jambsPrefs[2], new Vector3(transform.position.x + j * 2, transform.position.y + 2 * i, 0), transform.rotation);
                                break;
                            }
                        }
                        else
                        {
                            Instantiate(jambsPrefs[2], new Vector3(transform.position.x, transform.position.y + 2 * i, 0), transform.rotation);
                            break;
                        }
                    }
                }
                Instantiate(facedePref[Random.Range(1,3)], new Vector3(transform.position.x+j*2, transform.position.y + 2 * i, 0), transform.rotation);
            }

            for (int c = 0; c < chance.Length; c++)
            {
                if (rand < chance[c])
                {
                    if (onlySimleJambs[buildingMap.width-1, i]!=-1)
                    {
                        if (onlySimleJambs[buildingMap.width - 1, i] != 3)
                        {
                            Instantiate(jambsPrefs[c], new Vector3(transform.position.x + (buildingMap.width - 1) * 2, transform.position.y + 2 * i, 0), transform.rotation);
                            break;
                        }
                        else
                        {
                            Instantiate(jambsPrefs[2], new Vector3(transform.position.x + (buildingMap.width - 1) * 2, transform.position.y + 2 * i, 0), transform.rotation);
                            break;
                        }
                    }
                    else
                    {
                        Instantiate(jambsPrefs[2], new Vector3(transform.position.x + (buildingMap.width - 1) * 2, transform.position.y + 2 * i, 0), transform.rotation);
                        break;
                    }
                }
            }

            Instantiate(facedePref[4], new Vector3(transform.position.x + (buildingMap.width-1)*2, transform.position.y + 2 * i, 0), transform.rotation);
        }

        Instantiate(roofPref[0], new Vector3(transform.position.x, transform.position.y + buildingMap.height * 2, 0), transform.rotation);
        for (int j = 1; j < buildingMap.width - 1; j++)
        {
            Instantiate(roofPref[Random.Range(1, 4)], new Vector3(transform.position.x + j * 2, transform.position.y + buildingMap.height*2, 0), transform.rotation);
        }
        Instantiate(roofPref[4], new Vector3(transform.position.x + (buildingMap.width - 1) * 2, transform.position.y + buildingMap.height * 2, 0), transform.rotation);
        GameObject.Find("Player").transform.position = transform.position;

        for(int i = 1; i < 3; i++)
        {
            Instantiate(facedePref[0], new Vector3(transform.position.x , transform.position.y - i * yDistance, 0), transform.rotation);
            for (int j = 1; j < buildingMap.width - 1; j++)
            {
                Instantiate(facedePref[1], new Vector3(transform.position.x + j * xDistance, transform.position.y - i * yDistance, 0), transform.rotation);
            }
            Instantiate(facedePref[4], new Vector3(transform.position.x + (buildingMap.width-1)*xDistance, transform.position.y - i * yDistance, 0), transform.rotation);
        }

        Instantiate(doorsUp, new Vector3(transform.position.x + buildingMap.width / 2 * xDistance, transform.position.y - yDistance, 0), transform.rotation);
        Instantiate(doorDown, new Vector3(transform.position.x + buildingMap.width / 2 * xDistance, transform.position.y - 2 * yDistance, 0), transform.rotation);

        for(int i=0; i < buildingMap.width; i++)
        {
            int rand = (int)(Random.Range(0, 1.99f));
            Instantiate(bushes[rand], new Vector3(transform.position.x + i * xDistance, transform.position.y - 2 * yDistance, 0), transform.rotation);
        }

    }

    private void LoadWindowsFromImage()
    {
        windows = new IWindow[buildingMap.width , buildingMap.height];
        Color32[] pixels = buildingMap.GetPixels32();
        for(int i = 0; i < buildingMap.width; i++)
        {
            for(int j=0;j < buildingMap.height; j++)
            {
                bool used = false;
                for (int c = 0; c < 3; c++)
                {
                        if (pixels[i + j * buildingMap.width].Equals(LevelMenager.menager.windowTypes[c].windowColor))
                        {
                            GameObject go = Instantiate(LevelMenager.menager.windowTypes[c].window, new Vector3(transform.position.x + xDistance * i, transform.position.y + yDistance * j, 0), transform.rotation);
                            windows[i, j] = go.GetComponent<IWindow>();
                            windows[i, j].SetXY(i, j);
                            used = true;
                        if (c == 1)
                        {
                            onlySimleJambs[i, j] = 3;
                        }
                        if(c == 2)
                        {
                            GameObject go2 = Instantiate(soundCreatorPref, new Vector3(transform.position.x + xDistance * i, transform.position.y + yDistance * j, 0), transform.rotation);
                            go2.transform.SetParent(go.transform);
                            go2.GetComponent<AudioCrator>().myClip = disco;
                        }
                        }
                }
                if(pixels[i + j * buildingMap.width].Equals(LevelMenager.menager.windowTypes[3].windowColor))
                {
                    int height = 1;
                    onlySimleJambs[i, j] = -1;
                    while (pixels[i + j * buildingMap.width + height * buildingMap.width].Equals(LevelMenager.menager.windowTypes[3].windowColor))
                    {
                        pixels[i + j * buildingMap.width + height * buildingMap.width] = new Color32(0, 0, 0, 255);
                        onlySimleJambs[i, j+height] = -1;
                        height += 1;

                    }
                    GameObject go = Instantiate(LevelMenager.menager.windowTypes[3].window, new Vector3(transform.position.x + xDistance * i, transform.position.y+  yDistance * j, 0), transform.rotation);
                    // USTAWIC WYSKOSC JAZDY WINDY NA HEIGHT
                    windows[i,j] = go.GetComponent<IWindow>();
                    GameObject go2 = Instantiate(soundCreatorPref, new Vector3(transform.position.x + xDistance * i, transform.position.y + yDistance * j, 0), transform.rotation);
                    //go2.transform.SetParent(go.transform);
                    go2.GetComponent<AudioCrator>().myClip = elevetor;
                    go.GetComponent<WindowVertical>().audioGO = go2;

                    windows[i, j].SetXY(i, j);
                    windows[i, j].GetComponent<WindowVertical>().SetWindowData(height);
                    used = true;
                }
                if (pixels[i + j * buildingMap.width].Equals(LevelMenager.menager.windowTypes[4].windowColor))
                {
                    int horizontal = 1;
                    while (pixels[i + j * buildingMap.width + horizontal].Equals(LevelMenager.menager.windowTypes[4].windowColor))
                    {
                        pixels[i + j * buildingMap.width + horizontal] = new Color32(0, 0, 0, 255);
                        horizontal += 1;
                    }
                    GameObject go = Instantiate(LevelMenager.menager.windowTypes[4].window, new Vector3(transform.position.x + xDistance * i, transform.position.y +  yDistance * j, 0), transform.rotation);
                    // USTAWIC SZEROKOSC CHODZENIA NA VERT
                    windows[i,j] = go.GetComponent<IWindow>();
                    windows[i, j].SetXY(i, j);
                    GameObject go2 = Instantiate(soundCreatorPref, new Vector3(transform.position.x + xDistance * i, transform.position.y + yDistance * j, 0), transform.rotation);
                    //go2.transform.SetParent(go.transform);
                    go.GetComponent<WindowHorizontal>().audioGO = go2;
                    go2.GetComponent<AudioCrator>().myClip = grandma;
                    windows[i, j].GetComponent<WindowHorizontal>().SetWindowData(horizontal);
                    used = true;
                }
                if(pixels[i + j * buildingMap.width].Equals(LevelMenager.menager.endGoalColor))
                {
                    GameObject go = Instantiate(LevelMenager.menager.windowTypes[0].window, new Vector3(transform.position.x + xDistance * i, transform.position.y + yDistance * j, 0), transform.rotation);
                    windows[i, j] = go.GetComponent<IWindow>();
                    windows[i, j].SetXY(i, j);
                    onlySimleJambs[i, j] = -1;
                    go.transform.GetComponent<SpriteRenderer>().sprite = LevelMenager.menager.endGoalSprite;
                    endGoal.x = i;
                    endGoal.y = j;
                    used = true;
                }
                if (!used)
                {
                    GameObject go = Instantiate(windowNormalLightOn, new Vector3(transform.position.x + xDistance * i, transform.position.y + yDistance * j, 0), transform.rotation);
                    windows[i,j] = go.GetComponent<IWindow>();
                    windows[i, j].SetXY(i, j);
                }
            }
        }
    }
    
}
