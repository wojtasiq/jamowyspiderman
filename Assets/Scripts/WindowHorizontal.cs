﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class WindowHorizontal : IWindow {

	[SerializeField]
	private Sprite _windowOn;
	
	[SerializeField]
	private Sprite[] _person = new Sprite[4];

	[SerializeField] 
	private Sprite _windowOff;

	[SerializeField] private float _horizontalWaitFor = 2;
	[SerializeField] private float _horizontalWaitAtEnd = 3;

	private SpriteRenderer _renderer;

	public GameObject audioGO;
	
	[FormerlySerializedAs("horizontalElementsRight")] [SerializeField] private int _horizontalElementsRight = 1;
	[FormerlySerializedAs("currentPos")] [SerializeField] private int _currentPos = 0;
	[FormerlySerializedAs("lastPos")] [SerializeField] private int _lastPos = 0;
	[FormerlySerializedAs("walkingRight")] [SerializeField] private bool _walkingRight = true;
	
	
	

	public void SetWindowData(int right)
	{
		_horizontalElementsRight=right;
	}

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.sprite = _windowOn;
    }

    // Use this for initialization
    void Start ()
	{
        SetTurnedOn(false);

        _currentPos = 1;
		_lastPos = 1;
		
		StartCoroutine(waitToStart());
		
		
	}

	private IEnumerator waitToStart()
	{
		yield return new WaitForSeconds(Random.Range(0, _horizontalWaitFor));
		HorizotnalMainLoop();

	}

	private void HorizotnalMainLoop()
	{
		

		_lastPos = _currentPos;
		
		if (_currentPos==_horizontalElementsRight)
		{
			_walkingRight = false;
		}

		if (_currentPos==1)
		{
			_walkingRight = true;
		}
		
		if (_walkingRight)
		{
			_currentPos++;
		}
		else
		{
			_currentPos--;
		}

		if (_currentPos==1)
		{
			audioGO.transform.position = this.transform.position;
		}
		else
		{
			audioGO.transform.position = Building.Data.windows[GetX()+_currentPos-1, GetY()].transform.position;
		}
		
		
		StartCoroutine(HorizontalGrandmaCoorutin());

	}


	private IEnumerator HorizontalGrandmaCoorutin()
	{
		SpriteRenderer currentRenderer;
		if (_currentPos==1)
		{
			SetTurnedOn(true);
			currentRenderer = _renderer;
		}
		else
		{
			Building.Data.windows[GetX()+_currentPos-1, GetY()].GetComponent<WindowNormal>().SetTurnedOn(true);
			currentRenderer = Building.Data.windows[GetX()+_currentPos-1, GetY()].GetComponent<SpriteRenderer>();
		}

		StartCoroutine(NormalWindowCoorutineCoorutin());



		if (_currentPos!=1 && _currentPos!=_horizontalElementsRight)
		{
			if (_walkingRight)
			{
				currentRenderer.flipX = true;
			}
			
			currentRenderer.sprite = _person[0];
			yield return new WaitForSeconds(_horizontalWaitFor/4);
			currentRenderer.sprite = _person[1];
			yield return new WaitForSeconds(_horizontalWaitFor/4);
			currentRenderer.sprite = _person[2];
			yield return new WaitForSeconds(_horizontalWaitFor/4);
			currentRenderer.sprite = _person[3];
			yield return new WaitForSeconds(_horizontalWaitFor/4);
			
			if (_walkingRight)
			{
				currentRenderer.flipX = false;
			}
		}
		else
		{
			if (_currentPos==1)
			{
				currentRenderer.sprite = _person[0];
				yield return new WaitForSeconds(_horizontalWaitFor/4);
				currentRenderer.sprite = _person[1];
				yield return new WaitForSeconds(_horizontalWaitAtEnd);
				currentRenderer.flipX = true;
				currentRenderer.sprite = _person[2];
				yield return new WaitForSeconds(_horizontalWaitFor/4);
				currentRenderer.sprite = _person[3];
				yield return new WaitForSeconds(_horizontalWaitFor/4);
				currentRenderer.flipX = false;
			}
			
			if (_currentPos==_horizontalElementsRight)
			{
				currentRenderer.flipX = true;
				currentRenderer.sprite = _person[0];
				yield return new WaitForSeconds(_horizontalWaitFor/4);
				currentRenderer.sprite = _person[1];
				yield return new WaitForSeconds(_horizontalWaitAtEnd);
				currentRenderer.flipX = false;
				currentRenderer.sprite = _person[2];
				yield return new WaitForSeconds(_horizontalWaitFor/4);
				currentRenderer.sprite = _person[3];
				yield return new WaitForSeconds(_horizontalWaitFor/4);
			}
			
			
		}



		
		HorizotnalMainLoop();
	}


	private IEnumerator NormalWindowCoorutineCoorutin()
	{

		SetNormalWidowTexture(true);
		
		yield return new WaitForSeconds(_horizontalWaitFor);
		
		SetNormalWidowTexture(false);

	}
	
	private void SetNormalWidowTexture(bool turnedOn)
	{
		if (turnedOn)
		{
			if (_lastPos==1)
			{
				_renderer.sprite = _windowOn;
			}
			else
			{
				Building.Data.windows[GetX()+_lastPos-1, GetY()].GetComponent<SpriteRenderer>().sprite = _windowOn;
			}
		}
		else
		{
			if (_lastPos==1)
			{
				SetTurnedOn(false);
				_renderer.sprite = _windowOff;
			}
			else
			{
				Building.Data.windows[GetX()+_lastPos-1, GetY()].GetComponent<WindowNormal>().SetTurnedOn(false);
				Building.Data.windows[GetX()+_lastPos-1, GetY()].GetComponent<SpriteRenderer>().sprite = _windowOff;
			}
		}
		
	}

}
