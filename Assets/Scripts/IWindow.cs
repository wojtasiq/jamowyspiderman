﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IWindow : MonoBehaviour {

    private int x;

    private int y;

    [SerializeField] private bool turnedOn;


    public bool GetTurnedOn()
    {
        return turnedOn;
    }

    public int GetX()
    {
        return x;
    }

    public int GetY()
    {
        return y;
    }

    public virtual void SetTurnedOn(bool turnedOn)
    {
        this.turnedOn = turnedOn;
    }

    public virtual void SetTurnedOn(bool turnedOn, Sprite on)
    {
        this.turnedOn = turnedOn;
    }

    public void SetXY(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void SetX(int x)
    {
        this.x = x;
    }

    public void SetY(int y)
    {
        this.y = y;
    }
    
}
