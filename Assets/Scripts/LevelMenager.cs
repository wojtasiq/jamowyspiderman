﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenager : MonoBehaviour {

    public static LevelMenager menager;
    public int CurrenLevel;
    public Texture2D[] levels;
    public float[] time;
    public ColorToWindow[] windowTypes;
    public Color32 endGoalColor;
    public Sprite endGoalSprite;


    // Use this for initialization
    void Awake () {
        if (menager == null)
        {
            menager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}


    public void LoadNextLevel()
    {
        if (CurrenLevel < levels.Length-1)
        {
            CurrenLevel++;
            SceneManager.LoadScene("game");
        }
        else{
            Debug.Log("WYGRALES");
            LoadWinScene();
        }
    }

    public void GoToFailLevel()
    {
        SceneManager.LoadScene("GameLost");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("GameStart");
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene("game");
    }

    public void LoadWinScene()
    {
        SceneManager.LoadScene("GameWin");
    }
        

}
