﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonFuncions : MonoBehaviour {

    public void NextLevel()
    {
        LevelMenager.menager.LoadNextLevel();
    }

    public void ResetLevel()
    {
        LevelMenager.menager.ResetLevel();
    }
}
