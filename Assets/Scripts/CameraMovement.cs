﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    private Transform playerPosition;
    public float followParam = 1f;
    private float offset = 0.01f;
    public static float defaultSize = 5f;

    // Use this for initialization
	void Start () {
        playerPosition = GameObject.Find("Player").transform;
	}
    public void FollowPlayer()
    {
        StartCoroutine("Follow");   
    }

    IEnumerator Follow()
    {
        while (true)
        {
            if (!(transform.position.y < playerPosition.position.y + offset && transform.position.y > playerPosition.position.y - offset))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + followParam * (playerPosition.position.y - transform.position.y), -10);
            }
            if (!(transform.position.x < playerPosition.position.x + offset && transform.position.x > playerPosition.position.x - offset))
            {
                transform.position = new Vector3(transform.position.x + followParam * (playerPosition.position.x - transform.position.x),transform.position.y, -10);
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
