﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonFunctions : MonoBehaviour {

	public void LoadNextLevel()
    {
        LevelMenager.menager.LoadNextLevel();
    }

    public void RestartLevel()
    {
        LevelMenager.menager.ResetLevel();
    }
}
