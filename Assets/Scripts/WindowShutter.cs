﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class WindowShutter : IWindow
{
	[FormerlySerializedAs("shutterOpen")] [SerializeField]
	private Sprite _shutterOpen;

	[FormerlySerializedAs("shutterClosed")] [SerializeField] 
	private Sprite _shutterClosed;

	[FormerlySerializedAs("shutterWaitFor")] [SerializeField] private float _shutterWaitFor = 3;

	private SpriteRenderer _renderer;
	
	// Use this for initialization
	void Start ()
	{
		_renderer = GetComponent<SpriteRenderer>();
		SetTurnedOn(false);
		_renderer.sprite = _shutterClosed;

		StartCoroutine(InitialShutterCoorutin());
		
		
	}
	
	
	private IEnumerator InitialShutterCoorutin()
	{
		
		
		yield return new WaitForSeconds(Random.Range(0, _shutterWaitFor));
		
		SetTurnedOn(!GetTurnedOn());
		SetWidowTexture();
		StartCoroutine(ShutterCoorutin());
	}

	private IEnumerator ShutterCoorutin()
	{
		yield return new WaitForSeconds(_shutterWaitFor);
		
		SetTurnedOn(!GetTurnedOn());
		SetWidowTexture();
		StartCoroutine(ShutterCoorutin());
	}

	private void SetWidowTexture()
	{
		if (GetTurnedOn())
		{
			_renderer.sprite = _shutterOpen;
		}
		else
		{
			_renderer.sprite = _shutterClosed;
		}
		
	}
	
	
}
