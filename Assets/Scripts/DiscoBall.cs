﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoBall : IWindow {

    private float startDelayMax = 0.5f;
    private float startDelay;
    private float delay = 0.75f;

    private SpriteRenderer ball;


    public Sprite neighbourLightedLeft;
    public Sprite neighbourLightedRight;

    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = transform.GetComponent<Animator>();
        startDelay = Random.Range(0, startDelayMax);
        ball = transform.GetComponent<SpriteRenderer>();
        StartCoroutine("letsParty");
	}
	
    IEnumerator letsParty()
    {
        anim.SetInteger("state",0);

        yield return new WaitForSeconds(startDelay);

        while (true)
        {
            if (this.GetX() > 0)
            {
                Building.Data.windows[this.GetX() - 1, this.GetY()].SetTurnedOn(false,neighbourLightedLeft);
            }
            if (this.GetX() < Building.Data.GetBuildingWidth()-1)
            {
                Building.Data.windows[this.GetX() + 1, this.GetY()].SetTurnedOn(false,neighbourLightedRight);
            }

            for (int i = 0; i < 2; i++)
            {
                this.SetTurnedOn(false);
                anim.SetInteger("state", 1);
                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds(delay);
                anim.SetInteger("state", 0);
                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds(delay);
            }
            if (this.GetX() > 0)
            {
                Building.Data.windows[this.GetX() - 1, this.GetY()].SetTurnedOn(true,neighbourLightedLeft);
            }
            if (this.GetX() < Building.Data.GetBuildingWidth()-1)
            {
                Building.Data.windows[this.GetX() + 1, this.GetY()].SetTurnedOn(true,neighbourLightedRight);
            }
            this.SetTurnedOn(true);
            anim.SetInteger("state", 2);
            yield return new WaitForSeconds(delay * 4);
        }
    }
	
}
