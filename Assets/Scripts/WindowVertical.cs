﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class WindowVertical : IWindow {

	[SerializeField]
	private Sprite[] _elevator = new Sprite[4];// 0 - stop, 1 - Go, 2 - half up, 3 - half down

	[SerializeField] 
	private Sprite _windowOff;

	[SerializeField] private float _verticalWaitFor = 3;

	private SpriteRenderer _renderer;

	public GameObject audioGO;

	[FormerlySerializedAs("horizontalElementsRight")] [SerializeField] private int _verticalElementsTop = 1;
	[FormerlySerializedAs("currentPos")] [SerializeField] private int _currentPos = 0;
	[FormerlySerializedAs("lastPos")] [SerializeField] private int _lastPos = 0;
	[FormerlySerializedAs("walkingRight")] [SerializeField] private bool _walkingUp = true;
	
	
	

	public void SetWindowData(int up)
	{
		_verticalElementsTop=up;
	}
	
	// Use this for initialization
	void Start ()
	{
		_renderer = GetComponent<SpriteRenderer>();
		SetTurnedOn(false);
		_renderer.sprite = _elevator[0];

		_currentPos = 1;
		_lastPos = 1;

		StartCoroutine(waitToStart());

	}
	
	private IEnumerator waitToStart()
	{
		yield return new WaitForSeconds(Random.Range(0, _verticalWaitFor));
		VerticalMainLoop();

	}
	
	private void VerticalMainLoop()
	{
		_lastPos = _currentPos;
		
		if (_currentPos==_verticalElementsTop)
		{
			_walkingUp = false;
		}

		if (_currentPos==1)
		{
			_walkingUp = true;
		}
		
		if (_walkingUp)
		{
			_currentPos++;
		}
		else
		{
			_currentPos--;
		}

		if (_currentPos==1)
		{
			audioGO.transform.position = this.transform.position;
		}
		else
		{
			audioGO.transform.position = Building.Data.windows[GetX(), GetY()+_currentPos-1].transform.position;
		}

		StartCoroutine(VerticalCoorutin());

	}


	private IEnumerator VerticalCoorutin()
	{
		SpriteRenderer currentRenderer;
		if (_currentPos==1)
		{
			SetTurnedOn(true);
			currentRenderer = _renderer;
		}
		else
		{
			Building.Data.windows[GetX(), GetY()+_currentPos-1].GetComponent<WindowNormal>().SetTurnedOn(true);
			currentRenderer = Building.Data.windows[GetX(), GetY()+_currentPos-1].GetComponent<SpriteRenderer>();
		}

		StartCoroutine(lastWindowCoorutineCoorutin());


		if (_walkingUp)
		{
			currentRenderer.sprite = _elevator[3];
			yield return new WaitForSeconds(_verticalWaitFor / 4);
			currentRenderer.sprite = _elevator[1];
			yield return new WaitForSeconds(_verticalWaitFor / 4);
		}
		else
		{
			currentRenderer.sprite = _elevator[2];
			yield return new WaitForSeconds(_verticalWaitFor / 4);
			currentRenderer.sprite = _elevator[1];
			yield return new WaitForSeconds(_verticalWaitFor / 4);
		}

		if (_currentPos==1 || _currentPos==_verticalElementsTop)
		{
			currentRenderer.sprite = _elevator[0];
			yield return new WaitForSeconds(Random.Range(1, _verticalWaitFor)/2);
			currentRenderer.sprite = _elevator[1];
			yield return new WaitForSeconds(Random.Range(1, _verticalWaitFor)/2);
		}

		
		VerticalMainLoop();
	}


	private IEnumerator lastWindowCoorutineCoorutin()
	{
		SpriteRenderer currentRenderer;
		if (_lastPos==1)
		{
			SetTurnedOn(true);
			currentRenderer = _renderer;
		}
		else
		{
			Building.Data.windows[GetX(), GetY()+_lastPos-1].GetComponent<WindowNormal>().SetTurnedOn(true);
			currentRenderer = Building.Data.windows[GetX(), GetY()+_lastPos-1].GetComponent<SpriteRenderer>();
		}
		
		
		if (_walkingUp)
		{
			currentRenderer.sprite = _elevator[2];
			yield return new WaitForSeconds(_verticalWaitFor / 4);
			currentRenderer.sprite = _windowOff;
		}
		else
		{
			currentRenderer.sprite = _elevator[3];
			yield return new WaitForSeconds(_verticalWaitFor / 4);
			currentRenderer.sprite = _windowOff;
			
		}
		
		if (_lastPos==1)
		{
			SetTurnedOn(false);
		}
		else
		{
			Building.Data.windows[GetX(), GetY()+_lastPos-1].GetComponent<WindowNormal>().SetTurnedOn(false);
		}

	}
	
}
