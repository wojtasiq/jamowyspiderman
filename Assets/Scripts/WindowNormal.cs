﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowNormal : IWindow {

    public Sprite defaultTexture;
    public Sprite lighted;
    [SerializeField] private bool turnedOnForever = true; //domyslnie wlaczone

    private SpriteRenderer renderer;

    private void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = defaultTexture;
    }

    void Start()
    {
        base.SetTurnedOn(turnedOnForever);

    }

    public override void SetTurnedOn(bool turnedOn)
    {
        base.SetTurnedOn(turnedOn);
        if (turnedOn)
        {
            renderer.sprite = lighted;
        }
        else
        {
            renderer.sprite = defaultTexture;
        }
    }

    public override void SetTurnedOn(bool turnedOn, Sprite on)
    {
        base.SetTurnedOn(turnedOn);
        if (turnedOn)
        {
            renderer.sprite = on;
        }
        else
        {
            renderer.sprite = defaultTexture;
        }
    }


}
