﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    private int posX;
    private int posY;

    private int buildingWidth;
    private int buildingHeight;


    // odleglosc w swiecie gry miedzy oknami.
    private float xDistance;
    private float yDistance;

    // czas jaki ma uplynac na zmiane okna.
    public float movemantDealy;
    private Animator anim;

    private bool canMove;
    private bool dead;
    private Vector2 currentMove;

    ///------------------OBSLUGA GAME OVER
    //zmienne do liczenia czasu - jest tu tez tekst, co trochę bez sensu, że jest w playerze
    public Text textTime;
    private float currentTime;
    public float startTime = 30.0f;

    public Text textGameOver;
   
    public AudioClip gameOverSFX;
    public AudioClip gameWinSFX;

    public AudioClip []jumpSFXs;

    public void letMove()
    {
        canMove = true;
    }

    // Use this for initialization
    void Start () {
        buildingHeight = Building.Data.GetBuildingHeight();
        buildingWidth = Building.Data.GetBuildingWidth();
        xDistance = Building.Data.GetXDistance();
        yDistance = Building.Data.GetYDistance();
        currentMove = new Vector2(0,0);
        dead = false;
        canMove = false;
        startTime = LevelMenager.menager.time[LevelMenager.menager.CurrenLevel];
        anim = transform.GetComponent<Animator>();
        posX = posY = 0;

        // set the current time to the startTime specified
        currentTime = startTime;
        if (textGameOver) textGameOver.text = "";

        LookAtTarget.target = this.gameObject;
       
    }

    private void GetKeys()
    {
        if (currentMove.x == 0 && currentMove.y == 0)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                currentMove = new Vector2(0, 1);
                anim.SetInteger("State", 1);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                currentMove = new Vector2(0, -1);
                anim.SetInteger("State", -1);
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                currentMove = new Vector2(-1, 0);
                transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
                anim.SetInteger("State", 2);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                currentMove = new Vector2(1, 0);
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                anim.SetInteger("State", 2);
            }
            if( !(currentMove.x == 0 && currentMove.y == 0))
            {
                StartCoroutine("ChangePosition");
            }         
            

        }
    }

    IEnumerator ChangePosition()
    {

        yield return new WaitForSeconds(movemantDealy/5);
        StartCoroutine("MoveBody");
        yield return new WaitForSeconds(4*movemantDealy / 5);
        StopCoroutine("MoveBody");
        Move(currentMove.x, currentMove.y);
        currentMove = new Vector2(0, 0);
        anim.SetInteger("State", 0);
    }

    IEnumerator MoveBody()
    {
        Vector3 goal = new Vector3(transform.position.x + currentMove.x *xDistance, transform.position.y + currentMove.y*yDistance, 0);
        while (true)
        {
            if (currentMove.x > 0)
            {
                if (posX >= buildingWidth - 1)
                {
                    break;
                }
            }
            else if (currentMove.x < 0)
            {
                if (posX <= 0)
                {
                    break;
                }
            }
            if (currentMove.y > 0)
            {
                if (posY >= buildingHeight - 1)
                {
                    break;
                }
            }
            else if (currentMove.y < 0)
            {
                if (posY <= 0)
                {
                    break;
                }
            }
            transform.position = Vector3.Lerp(transform.position, goal , 0.5f);
            yield return new WaitForEndOfFrame();
        }
    }

    private void Move(float x, float y)
    {
        if (x > 0)
        {
            if (posX >= buildingWidth-1)
            {
                return;
            }
        }
        else if(x < 0)
        {
            if (posX <= 0)
            {
                return;
            }
        }
        if (y > 0)
        {
            if (posY >= buildingHeight-1)
            {
                return;
            }
        }
        else if ( y < 0)
        {
            if (posY <= 0)
            {
                return;
            }
        }
        posY += (int)(y);
        posX += (int)(x);
        transform.position = new Vector3(Building.Data.transform.position.x + posX * xDistance , Building.Data.transform.position.y+yDistance*posY,0);
        AudioSource.PlayClipAtPoint(jumpSFXs[Random.Range(0,6)], gameObject.transform.position);
    }

    private void CheckLights()
    {
        if (Building.Data.windows[posX, posY].GetTurnedOn())
        {
            // POSTAC UMIERA
            Debug.Log("Umarles, bo okno zapalone");
            canMove = false;
            currentMove = new Vector2(0, 0);
            dead = true;
            AudioSource.PlayClipAtPoint(gameOverSFX, gameObject.transform.position);
            GameOver();
        }
        if(Building.Data.GetEndGoal().x == posX && Building.Data.GetEndGoal().y == posY)
        {
            if (canMove)
            {
                AudioSource.PlayClipAtPoint(gameWinSFX, gameObject.transform.position);
                StartCoroutine("YouWon");
            }
            canMove = false;         
        }
    }

    //Sprawdza czy minął czas do spalenia
    private void CheckTime()
    {
        if (currentTime < 0)
        {
            dead = true;
            GameOver();
            AudioSource.PlayClipAtPoint(gameOverSFX, gameObject.transform.position);
        }
        else
        {
            currentTime -= Time.deltaTime;
            textTime.text = currentTime.ToString("0.00");
        }
    }

    IEnumerator YouWon()
    {
        anim.SetInteger("State", 7);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(1.5f);
        LevelMenager.menager.LoadNextLevel();
    }
	
	// Update is called once per frame
	void Update () {
        if (!dead)
        {
            if (canMove)
            {
                GetKeys();
            }
            CheckTime();
            CheckLights();           
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            LevelMenager.menager.LoadMenu();
        }
    }

    void GameOver()
    {

        StartCoroutine("Fail");
    }

    IEnumerator Fail()
    {
        anim.SetInteger("State", -10);
        // pojawienie sie napisu game over
        yield return new WaitForSeconds(2.5f);
        LevelMenager.menager.GoToFailLevel();
        textGameOver.text = "GAME OVER";
    }

}
