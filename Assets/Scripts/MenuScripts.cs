﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScripts : MonoBehaviour {

	public void StartGame()
    {
        SceneManager.LoadScene("game");
    }

    public void Credits()
    {
        SceneManager.LoadScene("credits");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Menu()
    {
        SceneManager.LoadScene("GameStart");
    }

    public void RestartLevel()
    {
        LevelMenager.menager.ResetLevel();
    }
}
