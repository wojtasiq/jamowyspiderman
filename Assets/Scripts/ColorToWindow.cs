﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorToWindow{
    public GameObject window;
    public Color32 windowColor;
}
