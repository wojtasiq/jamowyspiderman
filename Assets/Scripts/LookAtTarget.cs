﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour {

    static public GameObject target; // the target that the camera should look at

    public void StartLooking()
    {
        this.transform.position = (new Vector3(0.1f, 0.0f, -10.0f));
        if (target == null)
        {            
            target = this.gameObject;
            Debug.Log("Cel patrzenia nie jest określony. Domyślnie ustawiony na rodzica obiektu.");           
        }
        StartCoroutine("Look");
    }

    // Update is called once per frame
    IEnumerator Look()
    {
        while (true)
        {
            if (target)
            {
                transform.LookAt(target.transform);
            }
        yield return new WaitForEndOfFrame();
        }
    }
}
