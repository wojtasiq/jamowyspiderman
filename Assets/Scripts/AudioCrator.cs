﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCrator : MonoBehaviour {

    private Transform playerPosition;
    private AudioSource audio;
    public AudioClip myClip;
    private bool played = false;
    float close = 5;

	// Use this for initialization
	void Start () {
        playerPosition = GameObject.Find("Player").transform;
        audio = transform.GetComponent<AudioSource>();
        audio.volume = 0.5f;
        StartCoroutine("creatSound");
	}
	
	IEnumerator creatSound()
    {
        if(Mathf.Abs( Vector3.Distance(transform.position,playerPosition.position) ) < close && !played)
        {
            audio.PlayOneShot(myClip);
            played = true;
        }
        yield return new WaitForSeconds(0.1f);
        StartCoroutine("creatSound");
    }
}
